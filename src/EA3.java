import java.io.*;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class EA3 {
    // Variables generals
    private static final File file = new File("Departaments.dat");
    private static final String boldOn = "\033[1m";
    private static final String boldOff = "\033[0m";

    // MAIN
    public static void main(String[] args) throws InterruptedException, IOException {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        //Variables
        int id;
        String newEmployee;
        String newLocation;

        showMenu();
        int n = lector.nextInt();
        boolean exit = false;
        while (!exit) {
            switch (n) {
                case 1:
                    ex1();
                    break;
                case 2:
                    System.out.println("Introdueix nombre de departament a modificar:");
                    id = lector.nextInt();
                    System.out.println("Introdueix el nou nom:");
                    newEmployee = lector.next();
                    System.out.println("Introdueix la nova localitat");
                    newLocation = lector.next();
                    ex2(id, newEmployee, newLocation);
                    break;
                case 3:
                    System.out.println("Introdueix el departament a esborrar:");
                    id = lector.nextInt();
                    ex3(id);
                    break;
                case 4:
                    ex4();
                    break;
                case 0:
                    exit = true;
                    return;
                default:
                    System.out.println("Opció invàlida. Torna a introduir-ne una altra:");
                    break;
            }
            promptEnterKey();
            showMenu();
            n = lector.nextInt();
        }
    }

    // Obliga a presionar ENTER para avanzar.
    private static void promptEnterKey() throws InterruptedException {
        System.out.println("\nPrem \"ENTER\" per a continuar...");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    //MENU
    private static void showMenu() {
        System.out.println("\n\n\n\n\n");
        //Exericics
        System.out.println("FITXERS BINARIS:");
        System.out.println();
        //1
        System.out.println("1.- Crea l'arxiu \"Departaments.dat\".");
        //2
        System.out.println("2.- Modifica un departament");
        //3
        System.out.println("3.- Esborra un departament.");
        //4
        System.out.println("4.- Mostra la llista de departaments.");
        System.out.println();
        //Exit
        System.out.println("0.- SORTIR");
    }

    /**
     * 1.- Crear un fitxer binari per guardar dades de departaments, el nom del fitxer és "Departaments.dat".
     * Introdueix com a mínim 10 departaments. Les dades per cada departament són: Número de departament: enter,
     * Nom: String i Localitat:String
     * @throws IOException
     */
    private static void ex1() throws IOException {
        // En cas de que no existeixi el fitxer
        if (!file.exists()) {
            // Informació base del fitxer
            int[] departments = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // Departaments
            String[] employees = {
                    "Marta", "David", "Ferran", "Alba", "Clara",
                    "Didac", "Daniel", "Alicia", "Joan", "Andrea"}; // Noms
            String[] location = {
                    "Mataró", "Piera", "Molins de Rey", "Gavà", "Igualada",
                    "Cornellà de Llobregat", "Collbató", "Martorell", "Masquefa", "Abrera"}; // Localitats

            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file)); // Flux de sortida per inserir dades sobre el fitxer

            for (int i = 0; i < 10; i++) {
                dataOS.writeInt(departments[i]); // Afegim els departaments al fitxer
                dataOS.writeUTF(employees[i]); // Afegim els noms
                dataOS.writeUTF(location[i]); // Afegim les localitats
            }
            dataOS.close(); // Tanquem el flux de dades
            System.out.println("Fitxer " + boldOn + file.getName() + boldOff + " creat correctament.");
        } else
            System.out.println("El fitxer " + boldOn + file.getName() + boldOff + " ja existeix.");
    }

    /**
     * 2.- Modificar les dades d'un departament. El mètode rep per teclat el número de departament a modificar, el nou
     * nom de departament i la nova localitat. Si el departament no existeix, visualitza un missatge que ho indiqui.
     * Visualitza també les dades antigues del departament i les noves dades.
     * @param id Nombre del departament a modificar
     * @throws IOException
     */
    private static void ex2(int id, String newEmployee, String newLocation) throws IOException {
        // Llistes per guardar dades del fitxer
        ArrayList<Integer> departments = new ArrayList<>();
        ArrayList<String> employees = new ArrayList<>();
        ArrayList<String> locations = new ArrayList<>();

        DataInputStream dataIS = new DataInputStream(new FileInputStream(file)); // Flux d'entrada per extreure dades del fitxer
        // Guardem l'informació del fitxer a les llistes
        while (dataIS.available() > 0) {
            departments.add(dataIS.readInt());
            employees.add(dataIS.readUTF());
            locations.add(dataIS.readUTF());
        }
        dataIS.close(); // Tanquem el flux

        // Comprovem si el id existeix
        boolean idExists = false;
        for (int dep :
                departments) {
            if (dep == id) {
                idExists = true;
                break;
            }
        }

        String oldEmployee = null;
        String oldLocation = null;
        if (idExists) { // Si l'id existeix...
            for (int i = 0; i < departments.size(); i++) {
                if (id == departments.get(i)) {
                    oldEmployee = employees.get(i); // Guardem el nom antic
                    employees.set(i, newEmployee); // Afegim el nou nom

                    oldLocation = locations.get(i); // Guardem la localitat antiga
                    locations.set(i, newLocation); // Afegim la nova localitat
                    break;
                }
            }

            // Afegim les dades actualitzades al fitxer
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file, false)); // Flux de sortida per afegir dades del fitxer
            for (int i = 0; i < departments.size(); i++) {
                dataOS.writeInt(departments.get(i)); // Afegim departaments
                dataOS.writeUTF(employees.get(i)); // Afegim noms
                dataOS.writeUTF(locations.get(i)); // Afegim localitats
            }
            dataOS.close(); // Tanquem el flux

            System.out.println("Les dades s'han modificat correctament.\n");
            // Mostrem les dades antigues
            System.out.println("Dades antigues:");
            System.out.printf("\t→ %sDepartament:%s %s\n", boldOn, boldOff, id); // Departament
            System.out.printf("\t→ %sNom:%s %s\n", boldOn, boldOff, oldEmployee); // Nom
            System.out.printf("\t→ %sLocalitat:%s %s\n\n", boldOn, boldOff, oldLocation); // Localitat
            // Mostrem les dades actualitzades
            System.out.println("Dades actualitzades:");
            System.out.printf("\t→ %sDepartament:%s %s\n", boldOn, boldOff, id); // Departament
            System.out.printf("\t→ %sNom:%s %s\n", boldOn, boldOff, newEmployee); // Nom
            System.out.printf("\t→ %sLocalitat:%s %s\n\n", boldOn, boldOff, newLocation); // Localitat
        } else {
            System.out.println("L'empleat del departament " + id + " no existeix.");
        }


    }

    /**
     * 3.- Eliminar físicament un departament. El mètode rep pel teclat el número de departament a eliminar. Si el
     * departament no existeix, visualitza un missatge que ho indiqui. Visualitza també el número total de
     * departaments que existeixen en el fitxer.
     * @param id Departament a esborrar
     * @throws IOException
     */
    private static void ex3(int id) throws IOException {
        // Llistes per guardar dades del fitxer
        ArrayList<Integer> departments = new ArrayList<>();
        ArrayList<String> employees = new ArrayList<>();
        ArrayList<String> locations = new ArrayList<>();

        DataInputStream dataIS = new DataInputStream(new FileInputStream(file)); // Flux d'entrada per extreure dades del fitxer
        // Guardem l'informació del fitxer a les llistes
        while (dataIS.available() > 0) {
            departments.add(dataIS.readInt());
            employees.add(dataIS.readUTF());
            locations.add(dataIS.readUTF());
        }
        dataIS.close(); // Tanquem el flux

        // Comprovem si el id existeix
        boolean idExists = false;
        for (int dep :
                departments) {
            if (dep == id) {
                idExists = true;
                break;
            }
        }

        if (idExists) { // Si l'id existeix...
            for (int i = 0; i < departments.size(); i++) {
                if (id == departments.get(i)) {
                    departments.remove(i); // Esborrem el departament
                    employees.remove(i); // Esborrem el nom
                    locations.remove(i); // Esborrem la localitat
                    break;
                }
            }
            int count = departments.size();

            // Afegim les dades actualitzades al fitxer
            DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(file, false)); // Flux de sortida per afegir dades del fitxer
            for (int i = 0; i < departments.size(); i++) {
                dataOS.writeInt(departments.get(i)); // Afegim departaments
                dataOS.writeUTF(employees.get(i)); // Afegim noms
                dataOS.writeUTF(locations.get(i)); // Afegim localitats
            }
            dataOS.close(); // Tanquem el flux
            System.out.printf("Hi ha un total de %s%d%s departament/s.\n", boldOn, count, boldOff);
        } else {
            System.out.println("L'empleat del departament " + id + " no existeix.");
        }
    }

    /**
     * 4.- Necessitaràs també crear un mètode per llegir el fitxer binari creat.
     * @throws IOException
     */
    private static void ex4() throws IOException {
        DataInputStream dataIS = new DataInputStream(new FileInputStream(file)); // Flux d'entrada per extreure dades del fitxer

        // Variables que volem retornar
        int departments;
        String employees;
        String location;

        // Mentres que dataIS agafi valors en bytes majors que 0 (fins al final del fitxer)
        while (dataIS.available() > 0) {
            departments = dataIS.readInt(); // Llegim el departament
            employees = dataIS.readUTF(); // Llegim el nom
            location = dataIS.readUTF(); // Llegim la localitat

            System.out.printf("%sDepartament:%s %d\t,\t%sNom:%s %s\t,\t%sLocalitat:%s %s\n",
                    boldOn, boldOff, departments,
                    boldOn, boldOff, employees,
                    boldOn , boldOff, location);
        }
        dataIS.close(); // Tanquem el fitxer
    }
}
